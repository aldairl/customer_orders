from src.classesDao.product_dao import ProductDao 

class ProductController:

    @classmethod
    def select(cls):
        products = ProductDao.select()
        return products

    @classmethod
    def select_one(cls, id):
        product = ProductDao.select_by_id(id)
        return product

    @classmethod
    def update_price(cls, request_prod):
        product_id = request_prod['product_id']
        new_price = request_prod['price']
        product = ProductDao().select_by_id(product_id)
        product.price = new_price
        response = ProductDao().update_price(product)
        return response