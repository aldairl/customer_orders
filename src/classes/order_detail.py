
class OrderDetail:

    def __init__(self, order_id, product_id, product_description, price, quantity, order_detail_id=None):
        self._order_detail_id = order_detail_id
        self._order_id = order_id
        self._product_id = product_id
        self._product_description = product_description
        self._price = price
        self._quantity = quantity

    
    @property
    def order_detail_id(self):
        return self._order_detail_id

    @property
    def order_id(self):
        return self._order_id

    @property
    def product_id(self):
        return self._product_id

    @property
    def product_description(self):
        return self._product_description

    @property
    def price(self):
        return self._price

    @property
    def quantity(self):
        return self._quantity