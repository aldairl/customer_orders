from src.database.pool_cursor import PoolCursor
from src.classes.product import Product


class ProductDao:

    _INSERT = 'INSERT INTO product(product_id, name, description, price) values(%s, %s, %s, %s)'
    _SELECT = 'SELECT * FROM product ORDER BY product_id'
    _SELECT_BY_ID = 'SELECT * FROM product WHERE product_id = %s'
    _UPDATE = 'UPDATE product SET name=%s, description=%s, price=%s WHERE product_id = %s'
    _UPDATE_PRICE = 'UPDATE product SET price=%s WHERE product_id = %s'

    @classmethod
    def select(cls):
        with PoolCursor() as cursor:
            cursor.execute(cls._SELECT)
            reg_products = cursor.fetchall()
            products = []
            for reg_prod in reg_products:
                prod = Product(reg_prod[0], reg_prod[1],
                               reg_prod[2], reg_prod[3])
                products.append(prod.to_json())
            return products

    @classmethod
    def select_by_id(cls, product_id):
        with PoolCursor() as cursor:
            values = (product_id, )
            cursor.execute(cls._SELECT_BY_ID, values)
            reg_producut = cursor.fetchone()
            product1 = Product(
                reg_producut[0], reg_producut[1], reg_producut[2], reg_producut[3])
            return product1

    @classmethod
    def update_price(cls, product):
        with PoolCursor() as cursor:
            values = (product.price, product.product_id)
            cursor.execute(cls._UPDATE_PRICE, values)
            return cursor.rowcount
