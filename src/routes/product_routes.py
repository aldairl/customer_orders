from urllib import response
from flask import redirect, render_template, request, Blueprint, url_for
from src.classesDao.product_dao import ProductDao
from src.controllers.product_controller import ProductController

main = Blueprint('product_route', __name__)


@main.route('')
def get_products():
    try:
        response = ProductController.select()
        return {"success": True, "data": response}
    except Exception as e:
        return {"success": False, "message": str(e)}


@main.route('/admin/<int:id>')
def get_product(id):
    try:
        product = ProductController.select_one(id)
        return render_template('product_detail.html', product=product)

    except Exception as e:
        return {"success": False, "message": str(e)}


@main.route('', methods=['PUT', 'POST'])
def update_product():
    try:

        if request.method == 'POST':
            request_prod = request.form.to_dict()
            ProductController.update_price(request_prod)
            return redirect(url_for('product_route.show_products'))

        request_prod = request.json
        response = ProductController.update_price(request_prod)
        return {"success": True, "data": f'{response} product updated successfully'}

    except Exception as e:
        return {"success": False, "message": str(e)}


@main.route('/admin')
def show_products():
    try:
        products = ProductController.select()
        return render_template('products.html', products=products)
    except Exception as e:
        return {"success": False, "message": str(e)}
