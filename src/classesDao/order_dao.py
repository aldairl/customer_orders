from psycopg2 import Date
from src.database.pool_cursor import PoolCursor
from src.classes.order import Order

from src.util.logger_base import log
from src.util.format_date import DateFormat


class OrderDao:
    '''
    Dao - Data Access Object
    '''

    _SELECT = 'SELECT * FROM "order"'
    _INSERT = 'INSERT INTO "order"(customer_id, creation_date, delivery_address, total) VALUES( %s, %s, %s, %s) RETURNING order_id'
    _SELECT_BY_CUSTOMER_ID = ('SELECT creation_date, ord.order_id, total, delivery_address, quantity, name FROM "order" ord '
                              'INNER JOIN order_detail detail ON detail.order_id = ord.order_id '
                              'INNER JOIN product prod ON detail.product_id = prod.product_id '
                              'WHERE ord.customer_id = %s AND creation_date BETWEEN %s AND %s')

    @classmethod
    def select(cls):
        with PoolCursor() as cursor:
            cursor.execute(cls._SELECT)
            orders_reg = cursor.fetchall()
            orders = []

            for order_reg in orders_reg:
                order1 = Order(
                    order_reg[1], DateFormat.conver_date(order_reg[2]), order_reg[3], order_reg[4], order_reg[0]).__str__()
                orders.append(order1)
            return orders

    @classmethod
    def insert(cls, order_info):
        with PoolCursor() as cursor:
            values = (order_info.customer_id,
                      order_info.creation_date, order_info.delivery_address, order_info.total)
            cursor.execute(cls._INSERT, values)
            # return order_id
            return cursor.fetchone()[0]

    @classmethod
    def select_customer_orders_report(cls, customer_id, date1, date2):
        with PoolCursor() as cursor:
            values = (customer_id, date1, date2)
            cursor.execute(cls._SELECT_BY_CUSTOMER_ID, values)
            reg_orders = cursor.fetchall()

            customer_orders = []
            for reg_order in reg_orders:
                products = f'{reg_order[4]} x {reg_order[5]}'
                order = {
                    "creation_date": DateFormat.conver_date(reg_order[0]),
                    "order_id": reg_order[1], "total": reg_order[2],
                    "delivery_address": reg_order[3], "products": products
                }
                customer_orders.append(order)
            return customer_orders
