
class Order:
    def __init__(self, customer_id, creation_date, delivery_address, total, order_id=None):
        self._order_id = order_id
        self._customer_id = customer_id
        self._creation_date = creation_date
        self._delivery_address = delivery_address
        self._total = total

    def __str__(self):
        return {"order_id": self._order_id, "customer_id": self._customer_id, "creation_date": self._creation_date, "delivery_address": self._delivery_address, "total": self._total}

    @property
    def order_id(self):
        return self._order_id

    @property
    def customer_id(self):
        return self._customer_id

    @property
    def creation_date(self):
        return self._creation_date

    @property
    def delivery_address(self):
        return self._delivery_address

    @property
    def total(self):
        return self._total


if __name__ == '__main__':
    order1 = Order(123, 1059361303, '22/04/2022', 'calle 3j 52c-09', 25000)
    print(order1)
