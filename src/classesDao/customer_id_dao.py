from src.database.pool_cursor import PoolCursor
from src.classes.customer_product import CustomerProduct


class CustomerProductDao:

    _SELECT = 'SELECT * from customer_product'
    _SELECT_BY_CUSTOMER = 'SELECT * from customer_product WHERE customer_id = %s'

    @classmethod
    def selec(cls):
        with PoolCursor() as cursor:
            cursor.execute(cls._SELECT)
            registers = cursor.fetchall()
            customer_prods = []

            for cust_prd in registers:
                cust_prod = CustomerProduct(cust_prd[0], cust_prd[1]).__dict__
                customer_prods.append(cust_prod)
            return customer_prods

    @classmethod
    def selec_by_customer(cls, customer_id):
        with PoolCursor() as cursor:
            values = (customer_id,)
            cursor.execute(cls._SELECT_BY_CUSTOMER, values)
            registers = cursor.fetchall()
            customer_prods = []

            for cust_prd in registers:
                cust_prod = CustomerProduct(cust_prd[0], cust_prd[1])
                customer_prods.append(cust_prod)
            return customer_prods
