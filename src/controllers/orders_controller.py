from src.classes.order import Order
from src.classes.order_detail import OrderDetail
from src.classesDao.order_dao import OrderDao
from src.classesDao.order_detail_dao import OrderDetailDao
from src.classesDao.customer_id_dao import CustomerProductDao
from src.classesDao.product_dao import ProductDao
from src.util.logger_base import log

from decouple import config
from datetime import datetime


class OrderController:

    @classmethod
    def select(cls):
        return OrderDao.select()
    @classmethod
    def create_order(self, info_order):
        try:

            customer_id = info_order['customer_id']
            delivery_address = info_order['delivery_address']
            products_order = info_order['products']
            MAX_QUANTITY = int(config('MAX_QUANTITY'))

            # products permitted
            permitted_products_reg = CustomerProductDao().selec_by_customer(customer_id)
            permitted_products = [int(cp.product_id)
                                  for cp in permitted_products_reg]
            products_db = []

            for product in products_order:
                product_id = product['product_id']
                prod_quantity = product['quantity']

                if product_id not in permitted_products or prod_quantity > MAX_QUANTITY:
                    error = f'permitted products {str(permitted_products)} max quantity {MAX_QUANTITY}'
                    log.error(error)
                    raise ValueError(error)
                else:
                    productDB = ProductDao().select_by_id(product_id)
                    products_db.append(productDB)

            # get total order
            total = self.get_total_order(
                products_order=products_order, products=products_db)

            # create order
            creation_date = datetime.now()
            order1 = Order(customer_id, creation_date,
                           delivery_address, total)

            order_id = OrderDao().insert(order1)

            # create details order
            for product in products_db:
                product_id = product.product_id
                quantity = [
                    product['quantity'] for product in products_order if product['product_id'] == product_id
                ][0]

                description = product.description
                price = product.price
                order_detail1 = OrderDetail(
                    order_id, product_id, description, price, quantity)

                OrderDetailDao.insert(order_detail1)

            return f'insert row with id {order_id}'

        except Exception as e:
            log.error(f'error in create order {e}')
            raise e

    @classmethod
    def get_customer_orders_report(cls, customer_id, date1, date2):
        try:
            customer_orders = OrderDao.select_customer_orders_report(
                customer_id, date1, date2)

            if not len(customer_orders):
                raise ValueError(
                    f'customer {customer_id} has no orders in this range')

            return {"customer_id": customer_id, "orders": customer_orders}

        except Exception as e:
            log.error(f'Error in getting customer order {e}')
            raise e

    def get_total_order(products_order, products):

        log.info(products)
        # create tuple (quantity, price) of product
        quantity_prices = [(prod_info['quantity'], prod.price)
                           for prod_info in products_order for prod in products if prod_info['product_id'] == prod.product_id]
        total = 0
        for quantity_price in quantity_prices:
            total += quantity_price[0] * quantity_price[1]
        return total
