from src.database.pool_cursor import PoolCursor
from src.classes.order_detail import OrderDetail


class OrderDetailDao:
    _SELECT = 'SELECT * FROM order_detail'
    _INSERT = 'INSERT INTO "order_detail"(order_id, product_id, product_description, price, quantity) VALUES(%s, %s, %s, %s, %s) RETURNING order_detail_id'
    _SELECT_BY_ORDER_ID = 'SELECT * from "order_detail" WHERE order_id = %s'

    @classmethod
    def select(cls):
        with PoolCursor() as cursor:
            cursor.execute(cls._SELECT)
            reg_details = cursor.fetchall()
            details = []

            for reg_detail in reg_details:
                detail = OrderDetail(
                    reg_detail[1], reg_detail[2], reg_detail[3], reg_detail[4], reg_detail[5],reg_detail[0]).__dict__
                details.append(detail)

            return details

    @classmethod
    def insert(cls, order_detail):
        with PoolCursor() as cursor:
            values = (
                order_detail.order_id,
                order_detail.product_id, order_detail.product_description,
                order_detail.price, order_detail.quantity
            )
            cursor.execute(cls._INSERT, values)
            #return order_detail_id
            return cursor.fetchone()[0]
