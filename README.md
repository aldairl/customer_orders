# Customer Orders

Test project python3, web REST with Flask and postgresql.

## Installation

```
pip install -r requirements.txt
```

# Usage
Create and set environment development and entryPoint in file .env
```
FLASK_APP=src/run.py
FLASK_ENV=development
FLASK_RUN_HOST=localhost #optional
FLASK_RUN_PORT=5000 #optional
DATABASE=your_value
DB_PORT=your_value
USERNAME=your_value
PASSWORD=your_value
HOSTNAME=your_value
MIN_CONNECTION=your_value
MAX_CONNECTION=your_value
MAX_QUANTITY=5
```
## run aplication

```
flask run
```

## documentation
https://documenter.getpostman.com/view/20772701/UyrHeYNr

## License
MIT
