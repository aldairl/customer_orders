from src.util.logger_base import log
from psycopg2 import pool
from decouple import config


class ConnectionDB:
    _DATABASE = config('DATABASE')
    _USERNAME = config('USERNAME')
    _PASSWORD = config('PASSWORD')
    _DB_PORT = config('DB_PORT')
    _HOSTNAME = config('HOSTNAME')
    _MIN_CONNECTION = config('MIN_CONNECTION')
    _MAX_CONNECTION = config('MAX_CONNECTION')
    _pool = None

    @classmethod
    def get_pool(cls):

        if cls._pool is None:
            try:
                cls._pool = pool.SimpleConnectionPool(
                    cls._MIN_CONNECTION, cls._MAX_CONNECTION,
                    host=cls._HOSTNAME,
                    user=cls._USERNAME, password=cls._PASSWORD,
                    port=cls._DB_PORT, database=cls._DATABASE
                )
                return cls._pool

            except Exception as e:
                log.error(f'error in get pool connection {e}')
                raise str(e)
        else:
            return cls._pool

    @classmethod
    def get_connection(cls):
        connection = cls.get_pool().getconn()
        return connection

    @classmethod
    def close_connection(cls, connection):
        cls.get_pool().putconn(connection)

    @classmethod
    def close_all_connection(cls):
        cls.get_pool().closeall()
