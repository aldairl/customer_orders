import datetime

class DateFormat():

    @classmethod
    def conver_date(cls, date):
        return datetime.datetime.strftime(date, '%d-%m-%Y')