from flask import Flask
from .routes import orders_routes, product_routes

app = Flask(__name__)
app.register_blueprint(orders_routes.main, url_prefix='/api/order')
app.register_blueprint(product_routes.main, url_prefix='/api/product')

if __name__ == '__main__':
    app.run()