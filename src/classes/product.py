class Product:
    def __init__(self, product_id, name, description, price):
        self._product_id = product_id
        self._name = name
        self._description = description
        self._price = price

    def to_json(self):
        return {"product_id": self._product_id, "name": self._name, "description": self._description, "price": self._price}

    @property
    def product_id(self):
        return self._product_id

    @property
    def name(self):
        return self._name

    @property
    def description(self):
        return self._description

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, new_price):
        self._price = new_price
