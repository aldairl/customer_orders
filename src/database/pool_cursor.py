from .database_connection import ConnectionDB
from src.util.logger_base import log

class PoolCursor:
    def __init__(self):
        self._connection = None
        self._cursor = None

    def __enter__(self):
        self._connection = ConnectionDB.get_connection()
        self._cursor = self._connection.cursor()
        log.debug(f'__enter__ method {self._cursor}')
        #return self._cursor
        return self._connection.cursor()

    def __exit__(self, type_exception, exception_value, traceback):
        log.debug(f'In method __exit__ {traceback}')

        if exception_value:
            self._connection.rollback()
            log.error(f'error in exit method {exception_value} {type_exception}{traceback}')
        
        else:
            self._connection.commit()
            log.debug('commit transaction')

        self._cursor.close()
        ConnectionDB.close_connection(self._connection)