#from src import app
from flask import jsonify, render_template, request, Blueprint
from src.controllers.orders_controller import OrderController

main = Blueprint('order_blueprint', __name__)


@main.route('', methods=['GET', 'POST'])
def get_orders():
    try:
        response = ''
        if request.method == 'POST':
            request_order = request.get_json()
            response = OrderController.create_order(request_order)
        else:
            response = OrderController.select()
        return jsonify({"success": True, "data": response})

    except Exception as e:
        return jsonify({"success": False, "message": str(e)}), 500


# @main.route('/<int:customer_id>/<date1>/<date2>')
# def get_customer_orders(customer_id, date1, date2):
@main.route('/')
def get_customer_orders():
    try:
        customer_id = request.args.get('customer_id')
        date1 = request.args.get('date1')
        date2 = request.args.get('date2')
        response = OrderController.get_customer_orders_report(
            customer_id, date1, date2)
        return jsonify({"success": True, "data": response})

    except Exception as e:
        return jsonify({"success": False, "message": str(e)}), 500


@main.route('/report/')
def get_customer_orders_report():
    try:
        customer_id = request.args.get('customer_id')
        date1 = request.args.get('date1')
        date2 = request.args.get('date2')
        response = OrderController.get_customer_orders_report(
            customer_id, date1, date2)
        return render_template('table.html', products=response['orders'])

    except Exception as e:
        return jsonify({"success": False, "message": str(e)}), 500
